#!/usr/bin/env python3
"""
String Substitution for a Mad Lib
Adapted from code by Kirby Urner
"""                                                  

storyFormat = """                                       
Once upon a time, in the streets of {city},
there lived a {adjective} {animal}.  This {adjective} {animal}
liked to eat {food}, but {city} had
very little {food} to offer.  One day, a
garbageman found the {adjective} {animal} and discovered
it liked {food}.  The garbageman took the
{adjective} {animal} back to {city2}, where it could
eat as much {food} as it wanted.  However,
the {adjective} {animal} became homesick, so the
garbageman brought it back to {city},
leaving the {animal} a large supply of {food}.

The End
"""                                                 

def tellStory():                                     
    userPicks = dict()                              
    addPick('animal', userPicks)
    addPick('adjective', userPicks)
    addPick('food', userPicks)            
    addPick('city', userPicks)
    addPick('city2', userPicks)
            
    story = storyFormat.format(**userPicks)
    print(story)
                                                    
def addPick(cue, dictionary):
    '''Prompt for a user response using the cue string,
    and place the cue-response pair in the dictionary.
    '''
    prompt = 'Enter an example for ' + cue + ': '
    response = input(prompt).strip() # 3.2 Windows bug fix
    dictionary[cue] = response                                                             

tellStory()                                         
input("Press Enter to end the program.")        
