def createDictionary():
    numbers = dict()
    numbers['One'] = '1'
    numbers['Two'] = '2'
    numbers['Three'] = '3'
    numbers['Four'] = '4'
    return numbers

def main():
    dictionary = createDictionary()
    print(dictionary['One'])
    print(dictionary['Two'])
    print(dictionary['Three'])
    print(dictionary['Four'])

main()

