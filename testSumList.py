def sumList(nums):
    '''Return the sum of the numbers in the list nums.'''
    sum = 0
    for num in nums:
        sum = sum + num
    return sum


def main():
    print(sumList([5, 2, 4, 7]))
    print(sumList([]))
    print(sumList([100, 29, 3, -92]))
    print(sumList([-5, -10, 2, 3]))
main()


