def main():
    
    originalPrice = float(input('Enter the Original Price: '))
    discount = float(input('Please enter the discount: '))
    discountPrice = (1 - (discount/100))*originalPrice
    print('The price after the discount is  {0:.2f}'.format(discountPrice))
    
main()
